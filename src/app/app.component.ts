import { Component } from '@angular/core';
import { Messages } from './mock-message';
import { Message } from './message';
import { MessageComplete } from './mock-messageComplete';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-tudo';
  listMessage = Messages;
  message: Message;
  date: Date;
  isUpdate = false;
  displayAlert = 'none';
  valueMessage: string;
  messageComplete: Message[] = [];
  add(txtMessage: string) {
    if (txtMessage == '') {
      return;
    } else {
      if (this.check(txtMessage)) {
        this.displayAlert = 'block';
      } else {
        if(this.isUpdate == false)
        {
          this.date = new Date();
          this.listMessage.push(new Message(txtMessage, this.date));
          this.valueMessage = '';
        }else{
          // check up date
          var index = this.listMessage.findIndex(el=>el.content==this.message.content&&el.date==this.message.date);
          this.listMessage[index].content=txtMessage;
          this.listMessage[index].date=this.date = new Date();
          this.valueMessage = '';
          this.isUpdate = false;
        }

      }
    }
  }
  check(txtMessage: string): boolean {
    var count: number;
    count = 0;
    for (let message of this.listMessage) {
      console.log(message);
      if (message.content == txtMessage) {
        count++;
      }
    }
    console.log(count);
    if (count >= 1) {
      return true;
    } else {
      return false;
    }
  }
  addTwo(key: boolean, txtMessage: string) {
    if (key == true) {
      this.date = new Date();
      this.listMessage.push(new Message(txtMessage, this.date));
      this.displayAlert = 'none';
      this.valueMessage = '';
    } else {
      this.displayAlert = 'none';
    }
  }
  editMessage(txtMessage: string, date: Date) {
    this.isUpdate = true;
    this.valueMessage = txtMessage;
    this.message = new Message(txtMessage, date);
   
  }
  clear() {
    this.valueMessage = '';
    this.isUpdate = false;
  }
  delete(txtMessage: string, date: Date) {
    var index = this.listMessage.findIndex(el => el.content === txtMessage && el.date === date);
    this.listMessage.splice(index, 1);
  }
  addToComplete(message: string) {
    this.date = new Date();
    this.message = new Message(message, this.date);
    this.messageComplete.push(this.message);
    this.listMessage.splice(this.listMessage.findIndex(el => el.content === message), 1);
    console.log(this.messageComplete);
  }
}
