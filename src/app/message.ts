export class Message {
    content:string;
    date:Date;

    constructor(content?: string, date?: Date){
        this.content = content;
        this.date = date;
    }
    
}